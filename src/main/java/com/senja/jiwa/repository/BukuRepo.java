package com.senja.jiwa.repository;

import com.senja.jiwa.model.Buku;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BukuRepo extends JpaRepository<Buku, String> {

}
