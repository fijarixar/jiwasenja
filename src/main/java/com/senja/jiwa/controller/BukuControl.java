package com.senja.jiwa.controller;

import java.util.List;

import com.senja.jiwa.model.Buku;
import com.senja.jiwa.repository.BukuRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")
public class BukuControl {

    @Autowired
    private BukuRepo bukuRepo;

    // get semua buku

    @GetMapping("/buku")
    public List<Buku> getSemuaBuku() {
        return bukuRepo.findAll();
    }

    // tambah buku
    @PostMapping("/buku")
    public Buku tambahBukuBaru(@RequestBody Buku param) {
        bukuRepo.save(param);
        return param;
    }

}
