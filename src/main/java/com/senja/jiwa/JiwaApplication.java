package com.senja.jiwa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JiwaApplication {

	public static void main(String[] args) {
		SpringApplication.run(JiwaApplication.class, args);
	}

}
