package com.senja.jiwa.exception;

public class TidakDitemukan extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public TidakDitemukan(String pesan) {
        super(pesan);
    }
}
