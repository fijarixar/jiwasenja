package com.senja.jiwa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Buku")
public class Buku {

    // (strategy = GenerationType.SEQUENCE)

    // @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Id
    private String id;

    @Column(name = "judul_buku")
    private String Judul;

    @Column(name = "penulis_buku")
    private String Penulis;
    private String Tahun;

    public Buku() {
    }

    public Buku(String id, String Judul, String Penulis, String Tahun) {
        this.id = id;
        this.Judul = Judul;
        this.Penulis = Penulis;
        this.Tahun = Tahun;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJudul() {
        return this.Judul;
    }

    public void setJudul(String Judul) {
        this.Judul = Judul;
    }

    public String getPenulis() {
        return this.Penulis;
    }

    public void setPenulis(String Penulis) {
        this.Penulis = Penulis;
    }

    public String getTahun() {
        return this.Tahun;
    }

    public void setTahun(String Tahun) {
        this.Tahun = Tahun;
    }

    @Override
    public String toString() {
        return "{" +
                " id='" + getId() + "'" +
                ", Judul='" + getJudul() + "'" +
                ", Penulis='" + getPenulis() + "'" +
                ", Tahun='" + getTahun() + "'" +
                "}";
    }

}
