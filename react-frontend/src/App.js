import logo from './logo.svg';
import './App.css';
import ListBukuKomponen from './komponen/ListBukuKomponen';

function App() {
  return (
    <div className='container'>
        <ListBukuKomponen/>
    </div>
  );
}

export default App;
