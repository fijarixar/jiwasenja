import axios from 'axios';

const BUKU_API_BASE_URL = "http://localhost:8081/api/v1/buku";

class BukuServis {

    getBukuku() {
        
        return axios.get(BUKU_API_BASE_URL);
    }

}

export default new BukuServis()