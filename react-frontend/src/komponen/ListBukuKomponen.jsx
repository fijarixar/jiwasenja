import React, { Component } from 'react';
import BukuServis from '../Servis/BukuServis';

class ListBukuKomponen extends Component {
    constructor(props) {
        super(props)

        this.state = {
                bukuku: []
        }
    }

    componentDidMount() {
        BukuServis.getBukuku().then((res) =>{
            this.setState({ bukuku: res.data});
        });
    }

    render() {
        return (
            <div>
                <h2 className="text-center">List Buku</h2>
                <div className="row">
                    <table className="table table-striped table-bordered">

                        <thead>
                            <tr>
                                <th>Judul Buku</th>
                                <th>Penulis</th> 
                                <th>Tahun Terbit</th>
                                <th>aksi</th>
                                
                            </tr>
                        </thead>

                        <tbody>
                            {
                                this.state.bukuku.map(
                                    buku =>
                                    <tr key = {buku.id}>
                                        <td>{buku.judul}</td>
                                        <td>{buku.penulis}</td>
                                        <td>{buku.tahun}</td>
                                        
                                    </tr>
                                )
                            }
                        </tbody>

                    </table>
                </div>
            </div>
        );
    }
}

export default ListBukuKomponen;